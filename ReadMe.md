占位图生成服务，主要用于weex图片懒加载

### 安装

```
npm install
```
国内用户下载`canvas`可能报错，此时可以使用
```
npm install canvas --canvas_binary_host_mirror=https://registry.npmmirror.com/-/binary/canvas/
```
注意，不要直接把`node_modules`部署到服务器上，`node-canvas`在不同环境上构建的产物不同，直接运行可能会报错

### 运行
```
npm run start
```

### 使用
在浏览器地址栏输入：
```
http://127.0.0.1:3000/exc_w750_h3000.png?mw=750&mh=1624&bg=0xffcccccc
```
获得一张图片大小为750*3000，背景色为`rgba(204,204,204, 1)`的图片


路径说明：
- `w375` 图片宽度
- `h1500` 图片高度
- `mw=376` (可选)用于计算logo位置的图片宽度
- `mh=800` (可选)用于计算logo位置的图片高度
- `bg=0xffcccccc` (可选)图片背景色

`mw`和`mh`两个参数用于解决当原图片过长或过宽，生成的图片logo不能在第一屏展示的问题，一般可以设置为屏幕设计宽度和高度

`bg`参数为16进制的颜色，可以将`0x`以后的每两位数字视为`rgba()`中的`a`|`r`|`g`|`b`

注意：为避免占用过多服务器资源,图片宽度最大支持传入750,高度最大支持传入3000

### weex懒加载的例子
```html
<Image
  src="http://host/some-big-image.png"
  :style="{ width:750,height:3000 }"
  placeholder="http://127.0.0.1:3000/exc_w750_h3000.png?mw=750&mh=1624&bg=0xffcccccc"
></Image>

```